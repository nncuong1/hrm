package io.nnc.entity;

public enum Status {
	NEW("New"),
	INPROGRESS("Inprogress"),
	DONE("Done"),
	DELAY("Delay");
	
	private String name;
	
	private Status(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return name;
	}
}
