package io.nnc.entity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "Technology")
@Table(name = "technology")
@Setter
@Getter
public class Technology {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name", nullable = false, unique = true)
	@NaturalId
	private String name;

	@Column(updatable = false, name = "created_at")
	@CreationTimestamp
	private LocalDateTime createdAt;

	@Column(name = "updated_at")
	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	@ManyToMany(mappedBy = "technologies",fetch = FetchType.LAZY)
	private Set<User> users = new HashSet<>();
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Technology tech = (Technology) o;
		//return Objects.equals(id, tech.id);
		return Objects.equals(name.toLowerCase(), tech.name.toLowerCase());
	}

	@Override
	public int hashCode() {
		//return 31+id.hashCode();
		return Objects.hash(name.toLowerCase());
	}
}
