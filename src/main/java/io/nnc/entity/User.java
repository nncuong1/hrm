package io.nnc.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import javax.persistence.*;

import org.hibernate.annotations.CollectionId;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "User")
@Table(name = "user")
@Getter
@Setter
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "first_name", nullable = false)
	private String firstName;
	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Enumerated(EnumType.STRING)
	private Gender gender;

	@Column(name = "birth_date", columnDefinition = "DATE")
	private LocalDate birthDate;

	private Integer experience;

	private String avatar;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_id")
	private Department department;

	@Column(updatable = false, name = "created_at")
	@CreationTimestamp
	private LocalDateTime createdAt;

	@Column(name = "updated_at")
	@UpdateTimestamp
	private LocalDateTime updatedAt;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<UserProject> projects = new ArrayList<UserProject>();

	@ManyToMany(cascade = { CascadeType.MERGE })
	@JoinTable(name = "user_technology", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "technology_id"))
	private Set<Technology> technologies = new HashSet<Technology>();

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "document", joinColumns = @JoinColumn(name = "user_id"))
	@GenericGenerator(name="hilogen", strategy="increment")
	@CollectionId(columns = @Column(name = "document_id"), generator = "hilogen", type = @Type(type = "long"))
	private List<Document> documents = new ArrayList<Document>();

	public void addTech(Technology tech) {
		technologies.add(tech);
		tech.getUsers().add(this);
	}

	public void removeTech(Technology tech) {
		technologies.remove(tech);
		tech.getUsers().remove(this);
	}

	public void addProject(Project project, String status) {
		UserProject userProject = new UserProject(this, project, Status.valueOf(status));
		projects.add(userProject);
		// project.getUsers().add(userProject);
	}

	public void removeProject(Project project) {
		for (Iterator<UserProject> iterator = projects.iterator(); iterator.hasNext();) {
			UserProject userProject = iterator.next();

			if (userProject.getUser().equals(this) && userProject.getProject().equals(project)) {
				iterator.remove();
				userProject.getProject().getUsers().remove(userProject);
				userProject.setUser(null);
				userProject.setProject(null);
			}
		}
	}
}
