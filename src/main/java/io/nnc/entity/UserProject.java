package io.nnc.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "user_project")
@Getter
@Setter
public class UserProject {
	@EmbeddedId
	private UserProjectId id;

	@ManyToOne
	@MapsId("userId")
	private User user;

	@ManyToOne
	@MapsId("projectId")
	private Project project;

	@Enumerated(EnumType.ORDINAL)
	private Status status;
	
	@Column(updatable = false, name = "created_at")
	@CreationTimestamp
	private LocalDateTime createdAt;

	@Column(name = "updated_at")
	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public UserProject (User user, Project project,Status status) {
		this.user = user;
		this.project = project;
		this.id = new UserProjectId(user.getId(),project.getId());
		this.status = status;
	}
	public UserProject () {
	}
}
