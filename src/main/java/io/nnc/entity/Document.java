package io.nnc.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class Document {
	@Column(name = "file_name", nullable = false)
	private String fileName;

	@Column(name = "title", nullable = false)
	private String title;
	
	@Column(name = "extension", nullable = false)
	private String extension;
	
	@Column(name = "document_format",nullable = false)
	private String documentFormat;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Document other = (Document) o;
		if (!title.equals(other.title))
			return false;
		if (!extension.equals(other.extension))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = title.hashCode();
		result = 31 * result + extension.hashCode();
		return result;
	}
}
