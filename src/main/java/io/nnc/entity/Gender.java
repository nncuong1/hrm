package io.nnc.entity;

public enum Gender {
	MALE, FEMALE, OTHER
}
