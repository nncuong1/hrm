package io.nnc.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class UserProjectId implements Serializable {
	@Column(name = "user_id")
	private Long userId;
	@Column(name = "project_id")
	private Integer projectId;

	public UserProjectId() {
	}

	public UserProjectId(Long userId, Integer projectId) {
		super();
		this.userId = userId;
		this.projectId = projectId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		UserProjectId other = (UserProjectId) obj;
		return Objects.equals(getProjectId(), other.getProjectId()) && Objects.equals(getUserId(), other.getUserId());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((projectId == null) ? 0 : projectId.hashCode());
		return result;
	}
}
