package io.nnc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.nnc.payload.ApiResponse;
import io.nnc.payload.DepartmentDTO;
import io.nnc.payload.PagedResponse;
import io.nnc.service.DepartmentService;

@RestController
@RequestMapping("/api/v1/departments")
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;

	@GetMapping
	public ResponseEntity<PagedResponse<DepartmentDTO>> getAllDepartment() {
		PagedResponse<DepartmentDTO> response = departmentService.findAll();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getDepartmentById(@PathVariable("id") int id) {
		DepartmentDTO dto = departmentService.findById(id);
		return new ResponseEntity<>(dto, HttpStatus.OK);

	}

	@PostMapping
	public ResponseEntity<DepartmentDTO> createDepartment(@RequestBody DepartmentDTO departmentDto) {
		DepartmentDTO dto = departmentService.save(departmentDto);
		return new ResponseEntity<>(dto, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<ApiResponse> deleteDepartment(@PathVariable("id") int id) {
		ApiResponse response = departmentService.remove(id);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<DepartmentDTO> updateDepartment(@PathVariable("id") int id,
			@RequestBody DepartmentDTO newDepartment) {
		DepartmentDTO oldDepartment = departmentService.findById(id);
		newDepartment.setId(oldDepartment.getId());
		return new ResponseEntity<>(departmentService.update(newDepartment), HttpStatus.OK);

	}

}
