package io.nnc.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.nnc.payload.ApiResponse;
import io.nnc.payload.PagedResponse;
import io.nnc.payload.UserDTO;
import io.nnc.payload.UserSummary;
import io.nnc.service.DataMiner;
import io.nnc.service.UserService;
import io.nnc.service.impl.FileStorageServiceLocal;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
	static Logger log = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	
	//private DataMiner miner;
	
	@GetMapping("/user-summaries")
	public ResponseEntity<PagedResponse<UserSummary>> getAllUserSummary() {
		PagedResponse<UserSummary> userSummaries = userService.getAllUserSummanry();
		return new ResponseEntity<>(userSummaries, HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<PagedResponse<UserDTO>> getAllUserProfile() {
		PagedResponse<UserDTO> userProfile = userService.findAll();
		return new ResponseEntity<>(userProfile, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<UserDTO> getUserById(@PathVariable("id") long id) {
		UserDTO dto = userService.findById(id);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
//
	//@PostMapping(consumes={"multipart/form-data","application/json"})
	@PostMapping()
	public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {
		UserDTO dto = userService.save(userDTO);
		return new ResponseEntity<>(dto, HttpStatus.CREATED);
	}
	
//	@PostMapping(value = "/upload",consumes=MediaType.MULTIPART_FORM_DATA_VALUE)
//	public ResponseEntity<String> upload(@RequestPart("file") MultipartFile file) {
//		return new ResponseEntity<>(fileStorageService.uploadFile(file,null), HttpStatus.CREATED);
//	}
	
	@PostMapping(value = "/importer")
	public ApiResponse importUser(@RequestParam("file") MultipartFile file) {
		//miner.extracData(file);\
		userService.importUsertoDatabase(file);
		return new ApiResponse(Boolean.TRUE, "You successfully import for user : ");
	}
	
	@PostMapping(value = "/{id}/files")
	public ResponseEntity<ApiResponse> uploadDocument(@PathVariable("id") long id, @RequestParam("files") MultipartFile[] files) {
		ApiResponse response = userService.uploadUserDocuments(files, id);
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<ApiResponse> deleteUser(@PathVariable("id") long id) {
		ApiResponse response = userService.remove(id);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PatchMapping(value="/{id}",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<ApiResponse> uploadAvatar(@PathVariable("id") long id , @RequestParam(required = false) MultipartFile file) {
		ApiResponse response = userService.updateUserAvatar(id, file);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PutMapping(value="/{id}")
	public ResponseEntity<UserDTO> updateUser(@PathVariable("id") long id, @RequestBody UserDTO newUser) {
		UserDTO oldUser = userService.findById(id);
		newUser.setId(oldUser.getId());
		return new ResponseEntity<>(userService.save(newUser), HttpStatus.OK);
	}
//	
//	@DeleteMapping("/{userId}/technologies/{techId}")
//	public ResponseEntity<HttpStatus> deleteUserTechnology(@PathVariable("techId") int techId, @PathVariable("userId") long userId) {
//		try {
//			userService.removeTechnologyById(userId, techId);
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
}
