package io.nnc.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import io.nnc.entity.Project;
import io.nnc.payload.ProjectDTO;
import io.nnc.repository.ProjectRepository;
import io.nnc.service.ProjectService;
import io.nnc.service.impl.FileStorageServiceLocal;

@RestController
//@CrossOrigin(origins = "http://127.0.0.1:5500")
public class FileDownloadController {

	static Logger log = Logger.getLogger(FileDownloadController.class);

	@Autowired
	private FileStorageServiceLocal fileStorageService;

	@Autowired
	private ProjectService projectService;

	@GetMapping("/downloadFile/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
		// Load file as Resource
		Resource resource = fileStorageService.loadFileAsResource(fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			log.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	@GetMapping("/pdf")
	public ResponseEntity<Resource> downloadPdf(HttpServletRequest request) throws FileNotFoundException {
		// Load file as Resource
		String fileName= "TPBank.pdf";
		File file = new File(fileName);
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

//		return ResponseEntity.ok().headers(headers).contentLength(file.length())
//				.contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
				.contentLength(file.length())
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
				.body(resource);
	}

	@GetMapping("/csv")
	public ResponseEntity<Resource> downloadFilecsv() throws IOException {
		String fileName = "hello.csv";

//		List<List<String>> rows = Arrays.asList(
//			    Arrays.asList("Jean", "author", "Java"),
//			    Arrays.asList("David", "editor", "Python"),
//			    Arrays.asList("Scott", "editor", "Node.js")
//			);
//		FileWriter csvWriter = new FileWriter("new.csv");
//		csvWriter.append("Name");
//		csvWriter.append(",");
//		csvWriter.append("Role");
//		csvWriter.append(",");
//		csvWriter.append("Topic");
//		csvWriter.append("\n");
//		for (List<String> rowData : rows) {
//		    csvWriter.append(String.join(",", rowData));
//		    csvWriter.append("\n");
//		}
//		
//		csvWriter.flush();
//		csvWriter.close();

		// Load file as Resource
		// ByteArrayOutputStream out = new ByteArrayOutputStream();
		// Resource resource = new ByteArrayResource(out.toByteArray());
		// InputStreamResource file = new InputStreamResource(csvWriter.);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ICsvBeanWriter csvBeanWriter = new CsvBeanWriter(new PrintWriter(out), CsvPreference.STANDARD_PREFERENCE);

		String[] csvHeader = { "ID", "Name", "Description", "Start", "End" };
		String[] nameMapping = { "id", "name", "description", "dateStart", "dateEnd" };
		List<ProjectDTO> projects = projectService.findAll().getContent();
		csvBeanWriter.writeHeader(csvHeader);
		for (ProjectDTO project : projects) {
			csvBeanWriter.write(project, nameMapping);
		}
		csvBeanWriter.close();
		InputStreamResource file = new InputStreamResource(new ByteArrayInputStream(out.toByteArray()));

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
				.contentType(MediaType.parseMediaType("application/csv")).body(file);
	}
//	@GetMapping(value="/csv")
//	public String uploadCsv(@RequestParam("file") MultipartFile file) {
//		//new InputStreamReader(file.getInputStream())
//		BufferedReader csvReader;
//		try {
//			csvReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
//			String row;
//			String headerLine = csvReader.readLine();
//			while ((row = csvReader.readLine()) != null) {
//			    String[] data = row.split(",");
//			    Arrays.stream(data).forEach(System.out::println);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return "Upload";
//	}
}
