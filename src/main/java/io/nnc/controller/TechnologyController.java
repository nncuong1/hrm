package io.nnc.controller;

import java.util.Optional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.nnc.payload.ApiResponse;
import io.nnc.payload.PagedResponse;
import io.nnc.payload.TechnologyDTO;
import io.nnc.service.TechnologyService;

@RestController
@RequestMapping("/api/v1/technologies")
public class TechnologyController {

	static Logger log = Logger.getLogger(TechnologyController.class);

	@Autowired
	private TechnologyService technologyService;

	@GetMapping
	public ResponseEntity<PagedResponse<TechnologyDTO>> getAllTechnology() {
		PagedResponse<TechnologyDTO> technologyDtos = technologyService.findAll();
		if (technologyDtos.getContent().isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(technologyDtos, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<TechnologyDTO> getTechnologyById(@PathVariable("id") int id) {
		TechnologyDTO dto = technologyService.findById(id);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@RequestMapping(params = "name", method = RequestMethod.GET)
	public ResponseEntity<TechnologyDTO> getTechnologyByName(@RequestParam Optional<String> name) {
		TechnologyDTO dto = technologyService.findByName(name.get());
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<TechnologyDTO> createTechnology(@RequestBody TechnologyDTO TechnologyDto) {
		TechnologyDTO dto = technologyService.save(TechnologyDto);
		return new ResponseEntity<>(dto, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<TechnologyDTO> updateTechnology(@PathVariable("id") int id,
			@RequestBody TechnologyDTO newTechnology) {
		TechnologyDTO oldTechnology = technologyService.findById(id);
		newTechnology.setId(oldTechnology.getId());
		return new ResponseEntity<>(technologyService.update(newTechnology), HttpStatus.OK);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<ApiResponse> deleteTechnology(@PathVariable("id") int id) {
		ApiResponse response = technologyService.remove(id);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
