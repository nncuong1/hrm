package io.nnc.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.nnc.payload.ApiResponse;
import io.nnc.payload.PagedResponse;
import io.nnc.payload.ProjectDTO;
import io.nnc.service.ProjectService;

@RestController
@RequestMapping("/api/v1/projects")
public class ProjectController {

	@Autowired
	private ProjectService projectService;

	@GetMapping
	public ResponseEntity<PagedResponse<ProjectDTO>> getAllProject() {
		PagedResponse<ProjectDTO> projectDtos = projectService.findAll();
		return new ResponseEntity<>(projectDtos, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ProjectDTO> getProjectById(@PathVariable("id") int id) {
		ProjectDTO dto = projectService.findById(id);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<ProjectDTO> createProject(@RequestBody ProjectDTO ProjectDto) {
			ProjectDTO dto = projectService.save(ProjectDto);
			return new ResponseEntity<>(dto, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<ApiResponse> deleteProject(@PathVariable("id") int id) {
		ApiResponse response = projectService.remove(id);
		return new ResponseEntity<>(response,HttpStatus.OK);

	}

	@PutMapping("/{id}")
	public ResponseEntity<ProjectDTO> updateProject(@PathVariable("id") int id, @RequestBody ProjectDTO newProject) {
		ProjectDTO oldProject = projectService.findById(id);
		newProject.setId(oldProject.getId());
		return new ResponseEntity<>(projectService.update(newProject), HttpStatus.OK);
	}
}
