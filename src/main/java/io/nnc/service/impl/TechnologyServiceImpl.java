package io.nnc.service.impl;

import static io.nnc.util.Constants.ID;
import static io.nnc.util.Constants.NAME;
import static io.nnc.util.Constants.TECHNOLOGY;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.nnc.entity.Technology;
import io.nnc.exception.CustomException;
import io.nnc.exception.ResourceNotFoundException;
import io.nnc.payload.ApiResponse;
import io.nnc.payload.PagedResponse;
import io.nnc.payload.TechnologyDTO;
import io.nnc.repository.TechnologyRepository;
import io.nnc.service.TechnologyService;
import io.nnc.util.TechnologyConverter;

@Service
@Transactional
public class TechnologyServiceImpl implements TechnologyService  {
	
	static Logger log = Logger.getLogger(TechnologyServiceImpl.class);
	
	@Autowired
	private TechnologyRepository technologyRepo;

	@Autowired
	private TechnologyConverter technologyConverter;

	@Override
	public PagedResponse<TechnologyDTO> findAll() {
		List<TechnologyDTO> technologyDTOs =  technologyRepo.findAll()
													.stream()
													.map(technologyConverter::toDto)
													.collect(Collectors.toList());
		return new PagedResponse<>(technologyDTOs);
	}

	@Override
	public TechnologyDTO save(TechnologyDTO dto) {
		try {
			Technology technology = technologyRepo.save(technologyConverter.toEntity(dto));
			return technologyConverter.toDto(technology);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			throw new CustomException("Error when create Technology");
		}
	}

	@Override
	public TechnologyDTO update(TechnologyDTO newTechnology) {
		try {
			Technology technology = technologyRepo.save(technologyConverter.toEntity(newTechnology));
			return technologyConverter.toDto(technology);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			throw new CustomException("Error when create Technology");
		}
	}

	@Override
	public TechnologyDTO findById(Serializable id) {
		Technology technology =  technologyRepo.findById((Integer) id)
				.orElseThrow(() -> new ResourceNotFoundException(TECHNOLOGY, ID, id));
		return technologyConverter.toDto(technology);
	}

	@Override
	public ApiResponse remove(Serializable id) {
		Technology technology =  technologyRepo.findById((Integer) id)
				.orElseThrow(() -> new ResourceNotFoundException(TECHNOLOGY, ID, id));
		technologyRepo.delete(technology);
		return new ApiResponse(Boolean.TRUE, "You successfully deleted technology");
	}

	@Override
	public TechnologyDTO findByName(String name) {
		Technology technology = technologyRepo.findByName(name)
				.orElseThrow(() -> new ResourceNotFoundException(TECHNOLOGY, NAME, name));
		return technologyConverter.toDto(technology);
	}

}
