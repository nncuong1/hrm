package io.nnc.service.impl;

import static io.nnc.util.Constants.ID;
import static io.nnc.util.Constants.USER;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import io.nnc.entity.Document;
import io.nnc.entity.User;
import io.nnc.exception.CustomException;
import io.nnc.exception.ResourceNotFoundException;
import io.nnc.payload.ApiResponse;
import io.nnc.payload.PagedResponse;
import io.nnc.payload.UserDTO;
import io.nnc.payload.UserSummary;
import io.nnc.repository.UserRepository;
import io.nnc.service.FileStorageService;
import io.nnc.service.UserService;
import io.nnc.util.ExcelHelper;
import io.nnc.util.UserConverter;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	static Logger log = Logger.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepository userRepo;

	@Autowired
	private UserConverter userConverter;
	
	@Autowired
	private FileStorageService fileStorageService;

	@Override
	public PagedResponse<UserDTO> findAll() {
		// return
		List<UserDTO> userDTOs = userRepo.findAll().stream().map(userConverter::toDto).collect(Collectors.toList());
		return new PagedResponse<>(userDTOs);
	}

	@Override
	public PagedResponse<UserSummary> getAllUserSummanry() {
		List<UserSummary> userSummaries = userRepo.getAllUserSummary().stream().map(userConverter::toSumaryUser)
				.collect(Collectors.toList());
		return new PagedResponse<>(userSummaries);
	}

	@Override
	public UserDTO save(UserDTO dto) {
		try {
			User user = userConverter.toEntity(dto);
			return userConverter.toDto(userRepo.save(user));
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			throw new CustomException("Error when create or update User");
		}
	}

	@Override
	public UserDTO findById(Serializable id) {
		User user = userRepo.findById((Long) id).orElseThrow(() -> new ResourceNotFoundException(USER, ID, id));
		return userConverter.toDto(user);
	}

	@Override
	public ApiResponse remove(Serializable id) {
		User user = userRepo.findById((Long) id).orElseThrow(() -> new ResourceNotFoundException(USER, ID, id));
		userRepo.delete(user);
		return new ApiResponse(Boolean.TRUE, "You successfully deleted user");
	}

	@Override
	public ApiResponse updateUserAvatar(Long userId, MultipartFile file) {
		Optional<User> user = userRepo.findById(userId);
		if (!user.isPresent()) {
			throw new ResourceNotFoundException(USER, ID, userId);

		}
		String fileName = fileStorageService.storeFile(file,userId);
		userRepo.updateAvatarById(userId, fileName);
		return new ApiResponse(Boolean.TRUE, "You successfully updated avatar for user");
	}
	
	@Override
	public ApiResponse uploadUserDocuments(MultipartFile[] files, long userId) {
		Optional<User> user = userRepo.findById(userId);
		List<String> fileNames = new ArrayList<>();
		List<Document> docs = new ArrayList<>();
		if (!user.isPresent()) {
			throw new ResourceNotFoundException(USER, ID, userId);
		}
		log.info(user.get().getLastName());
		Arrays.asList(files).stream().forEach(file -> {
			String fileName =  fileStorageService.storeFile(file,userId);
			fileNames.add(fileName);
			Document doc = new Document();
			doc.setFileName(fileName);
			doc.setDocumentFormat(file.getContentType());
			doc.setExtension(fileName.substring(fileName.lastIndexOf(".")));
			doc.setTitle(fileName.substring(0,fileName.lastIndexOf(".")));
			docs.add(doc);
		});
		user.get().setDocuments(docs);
		return new ApiResponse(Boolean.TRUE, "You successfully updated document for user : "+fileNames);
	}


	@Override
	public UserDTO update(UserDTO t) {
		return null;
	}

	@Override
	public void removeTechnologyById(Long userId, Integer techId) {
		// User u = userRepo.getOne(userId);
		// log.info(userId+","+techId);
		// userRepo.deleteTechByUserIdAndTechId(userId, techId);
		// Technology tech = new Technology();
		// dieu kien xoa can trung voi phuong thuc equal cua entity (tehnology - id)
		// .setId(techId);
		// u.removeTech(tech);
	}

	@Override
	public void importUsertoDatabase(MultipartFile file) {
		// * getListUserDto from file
		// extract raw data from file
		// conver raw data to model
		try {
			List<UserDTO> dto = dto = ExcelHelper.excelToUser(file.getInputStream());
			List<User> users = dto.stream().map(userConverter::toEntity).collect(Collectors.toList());
			// save to db
			userRepo.saveAll(users);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * no use // project //List<Integer> projectIds =
	 * dto.getProjects().stream().map(project ->
	 * project.getProjectId()).collect(Collectors.toList()); //List<Project>
	 * projects = projectRepo.findByIdIn(projectIds); //
	 * dto.getProjects().stream().forEach(userProject -> { // Project p =
	 * projectRepo.getOne(userProject.getProjectId()); // saveUser.addProject(p,
	 * userProject.getStatus().toUpperCase()); // projects.stream().filter(project
	 * -> project.getId().equals(userProject.getProjectId())).forEach(p -> { //
	 * saveUser.addProject(p, userProject.getStatus().toUpperCase()); // }); // });
	 */
}
