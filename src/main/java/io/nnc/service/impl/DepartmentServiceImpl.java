package io.nnc.service.impl;

import static io.nnc.util.Constants.DEPARTMENT;
import static io.nnc.util.Constants.ID;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.nnc.entity.Department;
import io.nnc.exception.CustomException;
import io.nnc.exception.ResourceNotFoundException;
import io.nnc.payload.ApiResponse;
import io.nnc.payload.DepartmentDTO;
import io.nnc.payload.PagedResponse;
import io.nnc.repository.DepartmentRepository;
import io.nnc.service.DepartmentService;
import io.nnc.util.DepartmentConverter;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {
	static Logger log = Logger.getLogger(DepartmentServiceImpl.class);
	@Autowired
	private DepartmentRepository departmentRepo;

	@Autowired
	private DepartmentConverter departmentConverter;

	@Override
	public PagedResponse<DepartmentDTO> findAll() {
		List<DepartmentDTO> departmentDTOs = departmentRepo.findAll().stream().map(departmentConverter::toDto)
				.collect(Collectors.toList());
		return new PagedResponse<>(departmentDTOs);
	}

	@Override
	public DepartmentDTO save(DepartmentDTO dto) {
		try {
			dto.setName(null);
			Department department = departmentRepo.save(departmentConverter.toEntity(dto));
			return departmentConverter.toDto(department);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			throw new CustomException("Error when create Department");
		}
	}

	@Override
	public DepartmentDTO update(DepartmentDTO newDepartment) {
		try {
			Department department = departmentRepo.save(departmentConverter.toEntity(newDepartment));
			return departmentConverter.toDto(department);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			throw new CustomException("Error when create Department");
		}
	}

	@Override
	public DepartmentDTO findById(Serializable id) {
		Department department = departmentRepo.findById((Integer) id)
				.orElseThrow( () -> new ResourceNotFoundException(DEPARTMENT, ID, id));
		return departmentConverter.toDto(department);
	}

	@Override
	public ApiResponse remove(Serializable id) {
		Department department = departmentRepo.findById((Integer) id)
				.orElseThrow(() -> new ResourceNotFoundException(DEPARTMENT, ID, id));
		departmentRepo.delete(department);
		return new ApiResponse(Boolean.TRUE, "You successfully deleted post");
	}

}
