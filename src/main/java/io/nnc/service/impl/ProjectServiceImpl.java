package io.nnc.service.impl;

import static io.nnc.util.Constants.ID;
import static io.nnc.util.Constants.PROJECT;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.nnc.entity.Project;
import io.nnc.exception.CustomException;
import io.nnc.exception.ResourceNotFoundException;
import io.nnc.payload.ApiResponse;
import io.nnc.payload.PagedResponse;
import io.nnc.payload.ProjectDTO;
import io.nnc.repository.ProjectRepository;
import io.nnc.service.ProjectService;
import io.nnc.util.ProjectConverter;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService  {
	static Logger log = Logger.getLogger(ProjectServiceImpl.class);
	
	@Autowired
	private ProjectRepository projectRepo;

	@Autowired
	private ProjectConverter projectConverter;

	@Override
	public PagedResponse<ProjectDTO> findAll() {
		List<ProjectDTO>  projectDTOs= projectRepo.findAll()
				.stream()
				.map(projectConverter::toDto)
				.collect(Collectors.toList());
		return new PagedResponse<>(projectDTOs);
	}

	@Override
	public ProjectDTO save(ProjectDTO dto) {
		try {
			Project Project = projectRepo.save(projectConverter.toEntity(dto));
			return projectConverter.toDto(Project);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			throw new CustomException("Error when create Project");
		}
	}

	@Override
	public ProjectDTO update(ProjectDTO newProject) {
		try {
			Project Project = projectRepo.save(projectConverter.toEntity(newProject));
			return projectConverter.toDto(Project);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			throw new CustomException("Error when create Project");
		}
	}

	@Override
	public ProjectDTO findById(Serializable id) {
		Project project = projectRepo.findById((Integer) id)
				.orElseThrow(() -> new ResourceNotFoundException(PROJECT, ID, id));
		return projectConverter.toDto(project);
	}

	@Override
	public ApiResponse remove(Serializable id) {
		Project project = projectRepo.findById((Integer) id)
				.orElseThrow(() -> new ResourceNotFoundException(PROJECT, ID, id));
		projectRepo.delete(project);
		return new ApiResponse(Boolean.TRUE, "You successfully deleted project");
	}

}
