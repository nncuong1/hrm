package io.nnc.service.impl;

import static io.nnc.util.Constants.FILE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.nnc.FileStorageProperties;
import io.nnc.exception.FileStorageException;
import io.nnc.exception.ResourceNotFoundException;
import io.nnc.service.FileStorageService;

@Service
public class FileStorageServiceLocal implements FileStorageService {
	private final Path fileStorageLocation;
	static Logger log = Logger.getLogger(FileStorageServiceLocal.class);

	@Autowired
	public FileStorageServiceLocal(FileStorageProperties fileStorageProperties) {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDocument()).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}
	}
	
	@Override
	public String storeFile(MultipartFile file, Long id) {
		// Normalize file name
		String originalFileName = StringUtils.cleanPath(file.getOriginalFilename());
		String fileName = createFileName(originalFileName,id);
		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}
			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			//Files.write(targetLocation, file.getBytes());
			return fileName;
		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}
	
//	@Override
//	public String uploadFile(MultipartFile file, Long id) {
//		String avatarName = storeFile(file,id);
//		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/downloadFile/")
//                .path(avatarName)
//                .toUriString();
//		return fileDownloadUri;
//	}
	
	@Override
	public Resource loadFileAsResource(String fileName) {
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new ResourceNotFoundException(FILE, FILE, fileName);
			}
		} catch (MalformedURLException ex) {
			throw new ResourceNotFoundException(FILE, FILE, fileName);
		}
	}
	
	private String createFileName(String originalFileName,Long id) {
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");  
	    String formatDateTime = now.format(format);  
	    String fileName = id+"_"+formatDateTime+"_"+originalFileName;
		return fileName;
	}

}
