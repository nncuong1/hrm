package io.nnc.service;

import io.nnc.payload.DepartmentDTO;

public interface DepartmentService extends BaseService<DepartmentDTO>{

}
