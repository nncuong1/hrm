package io.nnc.service;

import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;
import io.nnc.payload.ApiResponse;
import io.nnc.payload.PagedResponse;
import io.nnc.payload.UserDTO;
import io.nnc.payload.UserSummary;

public interface UserService extends BaseService<UserDTO>{
	public void removeTechnologyById(Long userId,Integer id);
	public PagedResponse<UserSummary> getAllUserSummanry();
	public ApiResponse updateUserAvatar(Long userId, MultipartFile file);
	public ApiResponse uploadUserDocuments(MultipartFile [] files, long userId);
	public void importUsertoDatabase(MultipartFile file);
}
