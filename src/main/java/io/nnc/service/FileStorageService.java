package io.nnc.service;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;

public interface FileStorageService {
	public String storeFile(MultipartFile file, Long userId);
	public Resource loadFileAsResource(String fileName);
	//public String uploadFile(MultipartFile file, Long userId);
}
