package io.nnc.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public abstract class DataMiner {
	
	InputStream is;
	
	final void miner(MultipartFile file) {
		init(file);
	}
	
	public void init(MultipartFile file) {
		try {
			this.is = file.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	abstract void extractData();
	abstract void parseData();
	
}
