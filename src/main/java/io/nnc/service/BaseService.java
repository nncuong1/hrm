package io.nnc.service;

import java.io.Serializable;

import io.nnc.payload.ApiResponse;
import io.nnc.payload.PagedResponse;

public interface BaseService<T> {
	PagedResponse<T> findAll();

	T findById(Serializable id);

	T save(T t);
	
	T update(T t);

	ApiResponse remove(Serializable id);
}
