package io.nnc.service;

import io.nnc.payload.TechnologyDTO;

public interface TechnologyService extends BaseService<TechnologyDTO>{
	public TechnologyDTO findByName(String name);
}
