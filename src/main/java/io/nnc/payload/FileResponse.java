package io.nnc.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileResponse {
	private String name;
	private String url;

	public FileResponse(String name, String url) {
		this.name = name;
		this.url = url;
	}
	public FileResponse() {
		
	}
}
