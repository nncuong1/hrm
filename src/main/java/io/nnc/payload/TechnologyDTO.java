package io.nnc.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TechnologyDTO {
	private Integer id;
	private String name;
	
	public TechnologyDTO() {}

	public TechnologyDTO(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
}
