package io.nnc.payload;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Data;

@Data
public class PagedResponse<T> {
	
	private List<T> content;
	public PagedResponse() {

	}
	
	public PagedResponse(List<T> content) {
		this.content = content;
	}
	
	public List<T> getContent() {
		return content == null ? null : new ArrayList<>(content);
	}

	public final void setContent(List<T> content) {
		if (content == null) {
			this.content = null;
		} else {
			this.content = Collections.unmodifiableList(content);
		}
	}
}
