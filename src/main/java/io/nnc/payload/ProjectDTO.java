package io.nnc.payload;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProjectDTO {
	private Integer id;
	private String name;
	private String description;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private LocalDate dateStart;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private LocalDate dateEnd;
	private List<Long> userIds;

	public ProjectDTO() {
	}

	public ProjectDTO(Integer id, String name, String description, LocalDate dateStart, LocalDate dateEnd,List<Long> userIds) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.userIds = userIds;
	}
}
