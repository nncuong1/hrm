package io.nnc.payload;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder({
	"id","fullName","departmentName","avatar"
})
@Getter
@Setter
public class UserSummary {
	@JsonIgnore
	private String firstName;
	@JsonIgnore
	private String lastName;
	private Long id;
	private String avatar;
	private String fullName;
	private String department;
	
	public UserSummary() {
		
	}
	
	public UserSummary(String firstName, String lastName, String avatar, String department) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.avatar = avatar;
		this.department = department;
	}
	
	public void setFullName() {
		this.fullName = this.lastName.trim()+ " " + this.firstName.trim();
	}
}
