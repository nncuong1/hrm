package io.nnc.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserProjectDTO {
	private Integer projectId;
	private String status;
	
	public UserProjectDTO() {
		
	}
	
	public UserProjectDTO(String status, Integer projectId) {
		super();
		this.status = status;
		this.projectId = projectId;
	}
}
