package io.nnc.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepartmentDTO{
	private Integer id;
	private String name;
	private String code;
	
	public DepartmentDTO() {
	}
	
	public DepartmentDTO(Integer id, String name, String code) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
	}
}
