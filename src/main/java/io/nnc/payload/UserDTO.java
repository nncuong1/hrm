package io.nnc.payload;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDTO {
	private Long id;
	private String firstName;
	private String lastName;
	private String gender;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private LocalDate birthDate;
	private Integer experience;
	private String departmentCode;
	private String image;
	//@JsonManagedReference
	//private List<Integer> technologyIds;
	private List<String> technologyNames;
	public UserDTO() {

	}

	public UserDTO(Long id, String firstName, String lastName, String gender, LocalDate birthDate, Integer experience,
			String departmentCode, List<String> technologyNames) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthDate= birthDate;
		this.experience = experience;
		this.departmentCode = departmentCode;
		this.technologyNames = technologyNames;
	
	}
	public String getGender() {
		return gender.substring(0,1) + gender.substring(1).toLowerCase();
	}
}
