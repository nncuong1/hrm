package io.nnc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.nnc.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer>{
	
	public List<Project> findByIdIn(List<Integer> ids);
}
