package io.nnc.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.nnc.entity.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer>{
	
	public List<Department> findByCodeIn(List<String> codes);
	public Optional<Department> findByCode(String code);
}
