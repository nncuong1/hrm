package io.nnc.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.nnc.entity.Technology;

@Repository
public interface TechnologyRepository extends JpaRepository<Technology, Integer>{
	
	public Optional<Technology> findByName (String name);
	public List<Technology> findByIdIn(List<Integer> ids);
	public List<Technology> findByNameIn(List<String> names);
	
	@Modifying
	@Query(value="delete from user_technology where user_id = ?1",nativeQuery=true)
	public void deleteUserTechByUserId(Long userId);
}
