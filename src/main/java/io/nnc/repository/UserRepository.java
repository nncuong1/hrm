package io.nnc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import io.nnc.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	@Modifying
	@Query(value="delete from user_technology where user_id = ?1 and technology_id = ?2",nativeQuery=true)
	public void deleteTechByUserIdAndTechId(Long userId, Integer techId);
	
	@Modifying
	@Query(value="update user set avatar = :avatar where id = :id",nativeQuery=true)
	public void updateAvatarById(@Param(value = "id") Long userId, @Param(value = "avatar") String avatar);
	
	@Query(value="select u, d from User u join u.department d")
	public List<User> getAllUserSummary();
	
//	@Query(value="select u from User u join u.department d join u.projects p")
//	public List<User> getAllUserDetail();
}
