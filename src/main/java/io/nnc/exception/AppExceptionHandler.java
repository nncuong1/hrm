package io.nnc.exception;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.nnc.payload.ApiResponse;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler{
	
	static Logger log = Logger.getLogger(AppExceptionHandler.class);
//	
//	@ExceptionHandler(value = {Exception.class})
//	public ResponseEntity<Object> handleAnyException(Exception ex, WebRequest request){
//		String message = ex.getLocalizedMessage();
//		log.info("error : "+message);
//		ApiResponse response = new ApiResponse(Boolean.FALSE,message);
//		return new ResponseEntity<>(
//				response,new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
//	}
//	
//	@ExceptionHandler(value = {ResourceNotFoundException.class,BadRequestException.class,FileStorageException.class})
//	public ResponseEntity<Object> handleSpecificException(Exception ex, WebRequest request){
//		//ex.setApiResponse();
//		String message = ex.getLocalizedMessage();
//		ApiResponse response = new ApiResponse(Boolean.FALSE,message);
//		return new ResponseEntity<>(
//				response,new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
//	}

}
