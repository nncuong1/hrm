package io.nnc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CustomException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	private String message;

	public CustomException(String message) {
		super(message);
		this.message = message;
	}


	public String getMessage() {
		return message;
	}
}
