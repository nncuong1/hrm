package io.nnc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
//	@Value("${file.upload-img}") 
//	private String uploadImg;
	
	@Value("${file.upload-doc}") 
	private String uploadDocument;

//    public String getUploadImg() {
//        return uploadImg;
//    }
//
//    public void setUploadImg(String uploadImg) {
//        this.uploadImg = uploadImg;
//    }

	public String getUploadDocument() {
		return uploadDocument;
	}

	public void setUploadDocument(String uploadDocument) {
		this.uploadDocument = uploadDocument;
	}
}
