package io.nnc.util;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.nnc.entity.Gender;
import io.nnc.entity.Technology;
import io.nnc.entity.User;
import io.nnc.payload.UserDTO;
import io.nnc.payload.UserSummary;
import io.nnc.repository.DepartmentRepository;
import io.nnc.repository.TechnologyRepository;

@Component
public class UserConverter {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private TechnologyRepository techRepo;

	@Autowired
	private DepartmentRepository departmentRepository;

	public UserDTO toDto(User user) {
		UserDTO dto = modelMapper.map(user, UserDTO.class);
		dto.setGender(user.getGender().name());
		dto.setDepartmentCode(user.getDepartment().getCode());
		dto.setTechnologyNames(user.getTechnologies().stream().map(Technology::getName).collect(Collectors.toList()));
		if(user.getAvatar()!=null) {
			dto.setImage(getFileDownloadUri(user.getAvatar()));
		}
		return dto;
	}

	public User toEntity(UserDTO dto) {
		User user = modelMapper.map(dto, User.class);
		user.setGender(Gender.valueOf(dto.getGender().toUpperCase()));
		// handle relationship
		// department
		user.setDepartment(departmentRepository.findByCode(dto.getDepartmentCode()).get());
		// technology
		List<Technology> techs = techRepo.findByNameIn(dto.getTechnologyNames());
		techs.stream().forEach(tech -> user.getTechnologies().add(tech));
		return user;
	}

	public UserSummary toSumaryUser(User user) {
		UserSummary userSummary = modelMapper.map(user, UserSummary.class);
		userSummary.setFullName();
		userSummary.setDepartment(user.getDepartment().getName());
		if(user.getAvatar()!=null) {
			userSummary.setAvatar(getFileDownloadUri(user.getAvatar()));
		}
		return userSummary;
	}

	private String getFileDownloadUri(String imageName) {
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(imageName).toUriString();
		return fileDownloadUri;
	}
}
