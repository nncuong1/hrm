package io.nnc.util;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.nnc.entity.Project;
import io.nnc.payload.ProjectDTO;

@Component
public class ProjectConverter {

	@Autowired
    private ModelMapper modelMapper;
	
	public ProjectDTO toDto(Project project) {
		ProjectDTO dto = modelMapper.map(project, ProjectDTO.class);
		return dto;
	}

	public Project toEntity(ProjectDTO dto) {
		Project project = modelMapper.map(dto, Project.class);
		return project;
	}
}
