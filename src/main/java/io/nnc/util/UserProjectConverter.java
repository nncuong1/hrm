package io.nnc.util;

import org.springframework.stereotype.Component;

import io.nnc.entity.Status;
import io.nnc.entity.UserProject;
import io.nnc.entity.UserProjectId;
import io.nnc.payload.UserProjectDTO;

@Component
public class UserProjectConverter {

	public UserProjectDTO toDto(UserProject userProject) {
		UserProjectDTO dto = new UserProjectDTO();
		dto.setStatus(userProject.getStatus().toString());
		dto.setProjectId(userProject.getId().getProjectId());
		return dto;
	}

	public UserProject toEntity(UserProjectDTO dto) {
		UserProject userProject = new UserProject();
		UserProjectId id = new UserProjectId();
		userProject.setStatus(Status.valueOf(dto.getStatus().toUpperCase()));
		id.setProjectId(dto.getProjectId());
		userProject.setId(id);
		return userProject;
	}
}
