package io.nnc.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import io.nnc.payload.DepartmentDTO;
import io.nnc.payload.ProjectDTO;
import io.nnc.payload.UserDTO;

public class ExcelHelper {
	static Logger log = Logger.getLogger(ExcelHelper.class);
	public static List<ProjectDTO> excelToProject(InputStream is){return null;}
	public static List<DepartmentDTO> excelToDepartment(InputStream is){return null;}
	public static List<UserDTO> excelToUser(InputStream is) {
		try {
			Workbook workbook = new XSSFWorkbook(is);
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rows = sheet.iterator();
			List<UserDTO> dtos = new ArrayList<UserDTO>();

			// ignore header
			rows.next();

			// loop rows
			while (rows.hasNext()) {
				Row currentRow = rows.next();
				Iterator<Cell> cellsInRow = currentRow.iterator();
				UserDTO dto = new UserDTO();
				// loop cell
				int cellIndex = 0;
				while (cellsInRow.hasNext()) {
					Cell currentCell = cellsInRow.next();
					switch (cellIndex) {
						case 0:
							dto.setFirstName(currentCell.getStringCellValue());
							break;
						case 1: 
							dto.setLastName(currentCell.getStringCellValue());
							break;
						case 2 :
							dto.setGender(currentCell.getStringCellValue());
							break;
						case 3 :
							dto.setBirthDate(currentCell.getLocalDateTimeCellValue().toLocalDate());
							break;
						case 4 :
							dto.setExperience((int)currentCell.getNumericCellValue());
							break;
						case 5 :                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
							dto.setDepartmentCode(currentCell.getStringCellValue());
							break;
						case 6 : 
							String technologies = currentCell.getStringCellValue();
							dto.setTechnologyNames(Arrays.asList(technologies.substring(1,technologies.length()-1).split(",")));
						default:
							break;
					}
					cellIndex++;
				}
				dtos.add(dto);
			}
			workbook.close();
			return dtos;
		} catch (Exception e) {
			throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
		}
	}

//	private static Object getCellValue(Cell cell) {
//		switch (cell.getCellTypeEnum()) {
//			case BOOLEAN:
//				return cell.getBooleanCellValue();
//			case STRING:
//				return cell.getRichStringCellValue().getString();
//			case NUMERIC:
//				if (DateUtil.isCellDateFormatted(cell)) {
//					DataFormatter dataFormatter = new DataFormatter();
//					String date = dataFormatter.formatCellValue(cell);
//					log.info(date);
//					return date;
//				} else {
//					return cell.getNumericCellValue();
//				}
//			case FORMULA:
//				return cell.getCellFormula();
//			case BLANK:
//				return null;
//			default:
//				return null;
//		}
//	}
}
