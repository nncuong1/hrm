package io.nnc.util;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.nnc.entity.Technology;
import io.nnc.payload.TechnologyDTO;

@Component
public class TechnologyConverter {
	@Autowired
    private ModelMapper modelMapper;
	
	public TechnologyDTO toDto(Technology technology) {
		TechnologyDTO dto = modelMapper.map(technology, TechnologyDTO.class);
		return dto;
	}

	public Technology toEntity(TechnologyDTO dto) {
		Technology technology = modelMapper.map(dto, Technology.class);
		return technology;
	}
}
