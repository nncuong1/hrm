package io.nnc.util;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.nnc.entity.Department;
import io.nnc.payload.DepartmentDTO;

@Component
public class DepartmentConverter {
	
	@Autowired
    private ModelMapper modelMapper;

	public DepartmentDTO toDto(Department department) {
		DepartmentDTO dto = modelMapper.map(department, DepartmentDTO.class);	
		return dto;
	}

	public Department toEntity(DepartmentDTO dto) {
		Department department = modelMapper.map(dto, Department.class);
		return department;
	}
}
