package io.nnc.springboot.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import io.nnc.entity.Technology;
import io.nnc.entity.User;
import io.nnc.repository.ProjectRepository;
import io.nnc.repository.TechnologyRepository;
import io.nnc.repository.UserRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
class UserServiceImplTest {
	
	@Autowired
	private UserRepository userRepo;

	@Autowired
	private TechnologyRepository techRepo;

	@BeforeEach
	void setUp() throws Exception {
		
	}

	@Test
	@Rollback(true)
	void testRemoveTechnologyById() {
		Long userId = (long) 56;
		int techId = 2;
		User u = userRepo.findById(userId ).get();
		//log.info(userId+","+techId);
		//userRepo.deleteTechByUserIdAndTechId(userId, techId);
		Technology tech = new Technology();
		tech.setId(techId);
		u.removeTech(tech);
		assertThat(u.getTechnologies()).size().isEqualTo(1);
//		u.getTechnologies().remove(tech);
		
	}

}
